(in-package #:cl-user)

#+(and darwin sbcl) (eval-when (:compile-toplevel :load-toplevel :execute) (sb-int:set-floating-point-modes :traps nil))

(require :asdf)

(setf sb-impl::*default-external-format* :utf-8)

(defparameter *executable* t)
;;(defparameter *executable* nil)

(if *executable*
    (asdf:initialize-source-registry `(:source-registry (:tree ,(make-pathname :name nil :type nil
									       :defaults *load-pathname*))
							:ignore-inherited-configuration))
    (asdf:initialize-source-registry `(:source-registry (:tree #P"~/delivery/") :ignore-inherited-configuration)))

(asdf:load-system 'application-bundle)
(use-package 'application-bundle)
(require 'sb-posix)

(asdf:load-system 'cocoahelper)
(asdf:load-system 'lispbuilder-sdl-examples)
(asdf:load-system 'lispbuilder-sdl-ttf-examples)
(load #P"~/quicklisp/setup.lisp")
(ql:quickload :trivial-main-thread)
(ql:quickload :babel)
(ql:quickload :salza2)
(ql:quickload :chipz)
(ql:quickload :usocket)
(asdf:load-system 'xelf)
(asdf:load-system '3x0ng)

(load #P"~/quicklisp/setup.lisp")

(defun library-path (lib)
  (let* ((tmp (sb-posix:getcwd))
         (cwd (sb-ext:parse-native-namestring (if (char= (elt tmp (1- (length tmp))) #\/)
                                                  tmp
                                                  (format nil "~a/" tmp))))
         (executable (sb-ext:parse-native-namestring (first sb-ext:*posix-argv*)))
         (path (translate-pathname (if (eq (first (pathname-directory executable))
                                           :absolute)
                                       executable
                                       (make-pathname :directory (append
                                                                  (pathname-directory cwd)
                                                                  (cdr (pathname-directory executable)))
                                                      :defaults cwd))
                "/**/MacOS/*"
                (format nil "/**/Frameworks/~a.framework/~a" lib lib))))
    (probe-file path)))

(defun library-path* (lib)
  (sb-ext:parse-native-namestring (concatenate 'string "/Users/morbius/delivery/Frameworks/" lib ".framework/" lib)))

(defun lib (lib)
  (if *executable* (library-path lib) (library-path* lib)))

(defun register-libraries ()
  (sb-alien:load-shared-object (lib "cocoahelper"))
  (sb-alien:load-shared-object (lib "SDL"))
  (sb-alien:load-shared-object (lib "SDL_image"))
  (sb-alien:load-shared-object (lib "SDL_ttf"))
  (sb-alien:load-shared-object (lib "SDL_mixer"))
  (cffi:use-foreign-library cl-opengl-bindings::opengl)
  (lispbuilder-sdl-cocoahelper::cocoahelper-init)
  (setf lispbuilder-sdl-cffi::*image-loaded-p* t)
  (finish-output))

(defun find-arg (name)
    (let* ((args sb-ext:*posix-argv*)
	   (index (position name args :test 'equal))
	   (value (when (and (numberp index)
			     (< index (length args)))
		    (nth (1+ index) args))))
      value))

(defun install (&optional exe)
  (let* ((tmp (sb-posix:getcwd))
         (cwd (sb-ext:parse-native-namestring (if (char= (elt tmp (1- (length tmp))) #\/)
                                                  tmp
                                                  (format nil "~a/" tmp))))
         (executable (sb-ext:parse-native-namestring (first sb-ext:*posix-argv*)))
         (path (if (eq (first (pathname-directory executable))
                       :absolute)
                   executable
                   (make-pathname :directory (append
                                              (pathname-directory cwd)
                                              (cdr (pathname-directory executable)))
                                  :defaults cwd)))
         (projects (make-pathname :name nil :type nil
                                  :directory (append (butlast (pathname-directory path))
                                                     (list "Resources"
                                                           "projects"))
                                  :defaults path)))
    (register-libraries)
    ;; (sb-ext:disable-debugger)
    (if (not *executable*)
	(setf xelf:*project-directories* (list #P"~/delivery/" #P"~/delivery/xelf/"))
	(setf xelf:*project-directories* (list projects)))
      ;; (handler-bind
      ;;     ((style-warning #'muffle-warning))
    ;; (tmt:with-body-in-main-thread (:blocking T)
    ;;   (lispbuilder-sdl-cocoahelper::cocoahelper-init)
				      (3x0ng:3x0ng
				       :netplay
				       (let ((p (find-arg "--netplay")))
					 (when p 
					   (let ((*read-eval* nil))
					     (xelf::make-keyword (read-from-string p)))))
				       :use-upnp
				       (find-arg "--use-upnp")
				       :server-host
				       (find-arg "--server-host")
				       :client-host
				       (find-arg "--client-host")
				       :base-port
				       (let ((p (find-arg "--base-port")))
					 (when p
					   (let ((*read-eval* nil))
					     (read-from-string p))))
				       :verbose-logging
				       (find-arg "--verbose-logging"))))


(defun dump ()
  (sb-ext:save-lisp-and-die (write-application-bundle (make-pathname :name nil :type nil
                                                                   :defaults *load-pathname*)
                                                    :executable "3x0ng"
                                                    ;; :icon-file (make-pathname :name "3x0ng-icon"
                                                    ;;                           :type "icns"
                                                    ;;                           :defaults *load-pathname*)
                                                    :name "3x0ng")
                          :toplevel #'(lambda () (tmt:call-in-main-thread #'install :blocking t))
                          :executable t))

(when *executable* (dump))
