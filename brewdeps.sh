#!/bin/sh

/usr/local/bin/brew remove sdl sdl_gfx sdl_ttf sdl_image sdl_mixer

/usr/local/bin/brew install --universal --with-test sdl
/usr/local/bin/brew install --universal --with-test sdl_gfx
/usr/local/bin/brew install --universal --with-test sdl_ttf
/usr/local/bin/brew install --universal --with-test sdl_image
/usr/local/bin/brew install  --universal --with-test --with-libvorbis sdl_mixer 
