(in-package #:asdf)

(defsystem application-bundle
    :description "Create a Mac OS X application bundle"
    :version "0.0.1"
    :author "Shawn Betts"
    :components ((:file "application-bundle")))
