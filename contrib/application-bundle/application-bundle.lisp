(defpackage #:application-bundle
  (:use :cl)
  (:export #:write-application-bundle
           #:resources-pathname))

(in-package #:application-bundle)

(defstruct document-type
  extensions name
  (os-types (list "****"))
  icon-file
  role)

(defvar *indentation* "")

(defun write-tag (stream name contents &rest keys)
  (format stream "~a<~a~{ ~a=\"~a\"~}>~a</~a>~%" *indentation* name keys contents name))

(defun write-kv (stream key value)
  (write-tag stream "key" key)
  (write-tag stream "string" value))

(defun write-open-tag (stream name &rest keys)
  (format stream "~a<~a~{ ~a=\"~a\"~}>~%" *indentation* name keys))

(defun write-close-tag (stream name)
  (format stream "~a</~a>~%" *indentation* name))

(defmacro with-tag ((stream name &rest keys) &body body)
  (let ((ret (gensym "RET")))
    `(let (,ret)
       (write-open-tag ,stream ,name ,@keys)
       (setf ,ret
             (let ((*indentation* (concatenate 'string *indentation* (string #\Tab))))
               ,@body))
       (write-close-tag ,stream ,name)
       ,ret)))

(defun write-key-array (stream key values)
  (write-tag stream "key" key)
  (with-tag (stream "array")
    (dolist (v values)
      (write-tag stream "string" v))))

(defun copy-file (from to)
  "A very crude function to copy a file"
  (with-open-file (in from :element-type '(unsigned-byte 8))
    (with-open-file (out to :element-type '(unsigned-byte 8)
                            :direction :output)
      (let* ((len (min (file-length in) (* 1024 1024)))
             (seq (make-array len :element-type '(unsigned-byte 8))))
        (loop for bytes = (read-sequence seq in)
              while (plusp bytes)
              do (write-sequence seq out :end bytes))))))

(defun write-application-bundle (path &key
                                        executable
                                        icon-file
                                        (document-types nil)
                                        (development-region "English")
                                        (identifier "com.unknown")
                                        (name "Lisp Application")
                                        (package-type "APPL")
                                        (signature "????")
                                        (version "0.0.0")
                                        (short-version-string version)
                                        (principal-class "NSApplication"))
  "Build the application bundle and return the path to the executable."
  (let* ((app (make-pathname :directory (append (or (pathname-directory path)
                                                    (list :relative))
                                                (list (format nil "~a.app" name)))
                             :defaults path))
         (contents (make-pathname :directory (append (pathname-directory app)
                                                     (list "Contents"))
                                  :defaults app))
         (macos (make-pathname :directory (append (pathname-directory contents)
                                                  (list "MacOS"))
                               :defaults contents))
         (resources (make-pathname :directory (append (pathname-directory contents)
                                                      (list "Resources"))
                                   :defaults contents)))
    (ensure-directories-exist macos)
    (ensure-directories-exist resources)
    (when icon-file
      (copy-file icon-file (make-pathname :name (pathname-name icon-file)
                                          :type (pathname-type icon-file)
                                          :defaults resources)))
    (with-open-file (pkg (make-pathname :name "PkgInfo" :type nil :defaults contents)
                         :direction :output)
      (format pkg "~a~a" package-type signature))
    (with-open-file (info (make-pathname :name "Info" :type "plist" :defaults contents)
                          :direction :output)
      (write-line "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" info)
      (write-line "<!DOCTYPE plist SYSTEM \"file://localhost/System/Library/DTDs/PropertyList.dtd\">" info)
      (with-tag (info "plist" "version" 0.9)
        (with-tag (info "dict")
          (when document-types
            (write-tag info "key" "CFBundleDocumentTypes")
            (with-tag (info "array")
              (dolist (type document-types)
                (with-tag (info "dict")
                  (write-key-array info "CFBundleTypeExtensions"
                                   (document-type-extensions type))
                  (write-kv info "CFBundleTypeName" (document-type-name type))
                  (write-key-array info "CFBundleTypeOSTypes" (document-type-os-types type))
                  (when (document-type-icon-file type)
                    (write-kv info "CFBundleTypeIconFile" (file-namestring (document-type-icon-file type)))
                    (copy-file (document-type-icon-file type)
                               (make-pathname :name (pathname-name (document-type-icon-file type))
                                              :type (pathname-type (document-type-icon-file type))
                                              :defaults resources)))
                  (write-kv info "CFBundleTypeRole" (document-type-role type))))))
          (write-kv info "CFBundleDevelopmentRegion" development-region)
          (write-kv info "CFBundleExecutable" executable)
          (when icon-file
            (write-kv info "CFBundleIconFile" (file-namestring icon-file)))
          (write-kv info "CFBundleIdentifier" identifier)
          (write-kv info "CFBundleInfoDictionaryVersion" 6.0)
          (write-kv info "CFBundleName" name)
          (write-kv info "CFBundlePackageType" package-type)
          (write-kv info "CFBundleSignature" signature)
          (write-kv info "CFBundleShortVersionString" short-version-string)
          (write-kv info "CFBundleVersion" version)
          (write-kv info "NSPrincipalClass" principal-class)
          (make-pathname :name executable :type nil :defaults macos))))))

(defun resources-pathname (path app-name)
  (make-pathname :directory (append (or (pathname-directory path)
                                        (list :relative))
                                    (list (format nil "~a.app" app-name) "Contents" "Resources"))
                 :defaults path))
