LISPDIR=/usr/local/bin
LISP=$LISPDIR/sbcl

NAME="3x0ng"

rm -rf $NAME.app
$LISP --no-sysinit --no-userinit --load bootslime.lisp

cp -a Frameworks $NAME.app/Contents/
mkdir -p $NAME.app/Contents/Resources/projects/3x0ng
cp -a xelf/standard $NAME.app/Contents/Resources/projects/
cp 3x0ng/COPYING 3x0ng/*.{wav,png,ogg,txt,ttf,otf} $NAME.app/Contents/Resources/projects/3x0ng
mkdir -p $NAME.app/Contents/Source
cp 3x0ng/*.lisp 3x0ng/*.asd xelf/*.lisp xelf/*.asd $NAME.app/Contents/Source
mkdir -p $NAME.app/Contents/Licenses
cp xelf/licenses/* $NAME.app/Contents/Licenses

